SMD = 37+64 = 101 parts
16 unique parts

Transistors
11x MMBF170
8x  BSS84

Resistors 0805 @ �1% max
8x  820k
8x  56k
8x  5.6k
3x  33
1x  3.3k
8x  12k
8x  150k
1x  10k

Capacitors 0805 @ 50V X7R
8x  470n
8x  330n
8x  2.2n
2x  1u
8x  100n

Capacitors 1210 @ 50V X7R
3x  10u

NC = Not Connected
